package com.dz.exo.gold.demo

import android.content.Context
import android.content.SharedPreferences

class SharedPreference {
    fun save(context: Context, text: String?, Key: String?) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = settings.edit()
        editor.putString(Key, text)
        editor.commit()
    }

    fun getValue(context: Context, Key: String?): String? {
        val settings: SharedPreferences
        var text: String? = ""
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        text = settings.getString(Key, "")
        return text
    }

    fun clearSharedPreference(context: Context) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = settings.edit()
        editor.clear()
        editor.commit()
    }

    fun removeValue(context: Context, value: String?) {
        val settings: SharedPreferences
        val editor: SharedPreferences.Editor
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = settings.edit()
        editor.remove(value)
        editor.commit()
    }

    companion object {
        private var sharedPreference: SharedPreference? = null
        const val PREFS_NAME = "PREFS"
        const val PREFS_KEY = "CONFIG_ID_KEY"
        val instance: SharedPreference?
            get() {
                if (sharedPreference == null) {
                    sharedPreference = SharedPreference()
                }
                return sharedPreference
            }
    }
}