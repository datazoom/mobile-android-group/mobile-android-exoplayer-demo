package com.dz.exo.gold.demo

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.datazoom.collector.gold.ExoPlayerCollector
import com.dz.collector.android.collector.DZEventCollector
import com.dz.collector.android.connectionmanager.DZBeaconConnector
import com.dz.collector.android.model.DatazoomConfig
import com.dz.collector.android.model.Event
import com.dz.exo.gold.demo.databinding.ActivityPlayerBinding
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.google.android.exoplayer2.util.MimeTypes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import java.util.*


class PlayerActivity : AppCompatActivity() {
    private val TAG: String = PlayerActivity::class.java.getCanonicalName()
    private var player: ExoPlayer? = null
    private var playWhenReady = true
    private var currentWindow = 0
    private var onStop = false

    private var adsLoader: com.google.android.exoplayer2.source.ads.AdsLoader? = null
    private var sdkConfigured = false
    private var url: String? = null

    private var adsUrl: String? = null
    private val stagingUrl = "https://stagingplatform.datazoom.io/beacon/v1/"
    private val productionUrl = "https://platform.datazoom.io/beacon/v1/"
    private var appVideoType = AppVideoType.MP4

    private var eventCollectorOne: ExoPlayerCollector? = null
    private var eventCollectorTwo: ExoPlayerCollector? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var isSubtitleOn = false

    private val viewBinding by lazy(LazyThreadSafetyMode.NONE) {
        ActivityPlayerBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(viewBinding.root)
        val configId =
            SharedPreference.instance!!.getValue(applicationContext, SharedPreference.PREFS_KEY)
        if (!configId!!.isEmpty()) {
            viewBinding.txtConfiguration.setText(configId)
        }
        viewBinding.btnPush.setVisibility(View.GONE);
        viewBinding.txtVersion.setText("Demo - " + BuildConfig.VERSION_NAME + " | Library - " + BuildConfig.VERSION_NAME + " | Framework - " + BuildConfig.VERSION_NAME);

        viewBinding.initilaizeSDKSwitch.setOnCheckedChangeListener({ compoundButton, checked ->
            if (checked && !sdkConfigured) {
                configureSDKAndPlayer()
            }
        })

        viewBinding.swPlayerMuted.setOnCheckedChangeListener({ compoundButton, checked ->
            if (checked) {
                player?.volume = 0.0f
                viewBinding.seekbar.progress = 0
            } else {
                player?.volume = 1.0f
                viewBinding.seekbar.progress = 10
            }
        })

        viewBinding.initilaizeSDKSwitchWithoutExoInstance.setOnCheckedChangeListener({ compoundButton, checked ->
            if (checked && !sdkConfigured) {
                viewBinding.videoView.setVisibility(View.GONE)
                configureSDKAndPlayerWithoutExoInstance()
            }
        })


        viewBinding.btnSubmit.setOnClickListener({
            if (!sdkConfigured) {
                Toast.makeText(this, getString(R.string.sdk_initialized_msg), Toast.LENGTH_LONG)
                    .show()
            } else {
                viewBinding.settingsContainer.setVisibility(View.GONE)
                viewBinding.customDataPointSpinner.setVisibility(View.VISIBLE)
                viewBinding.btnSendCustomEvent.setVisibility(View.VISIBLE)
                viewBinding.btnPushCustomEvent.setVisibility(View.VISIBLE)
                viewBinding.btnSubmit.setEnabled(false)
                viewBinding.btnSubmit.setVisibility(View.GONE)
                viewBinding.videoView.setVisibility(View.VISIBLE)
                viewBinding.closeVideoButton.setVisibility(View.VISIBLE)
                if (appVideoType.equals(AppVideoType.HLS)) {
                    viewBinding.subtitlesButton.setVisibility(View.VISIBLE)
                } else {
                    viewBinding.subtitlesButton.setVisibility(View.INVISIBLE)
                }
                viewBinding.seekbar.visibility = View.VISIBLE
                Handler().postDelayed({
                    viewBinding.playAd.setOnClickListener { view ->
                        viewBinding.playAd.setVisibility(View.VISIBLE)
                        playAd()
                    }
                }, 5000)
                if (player != null) {
                    viewBinding.videoView.setVisibility(View.VISIBLE)
                    playVideo()
                } else {
                    viewBinding.videoView.setVisibility(View.GONE)
                }
            }

            Handler().postDelayed({

            }, 3000)

        })

        viewBinding.collectEventsSwitch.setOnCheckedChangeListener({ compoundButton, collectEvents ->
            eventCollectorOne?.startRecordingEvents(collectEvents)
        })


        viewBinding.closeVideoButton.setOnClickListener({
            viewBinding.settingsContainer.setVisibility(View.VISIBLE)
            viewBinding.customDataPointSpinner.setVisibility(View.GONE)
            viewBinding.btnSendCustomEvent.setVisibility(View.GONE)
            viewBinding.btnPushCustomEvent.setVisibility(View.GONE)
            viewBinding.btnSubmit.setVisibility(View.VISIBLE)
            viewBinding.btnSubmit.setEnabled(true)
            viewBinding.videoView.setVisibility(View.GONE)
            viewBinding.seekbar.setVisibility(View.GONE)
            viewBinding.playAd.setVisibility(View.GONE)
            viewBinding.closeVideoButton.setVisibility(View.INVISIBLE)
            viewBinding.subtitlesButton.setVisibility(View.INVISIBLE)
            viewBinding.initilaizeSDKSwitch.isChecked = false
            viewBinding.initilaizeSDKSwitchWithoutExoInstance.isChecked = false
            viewBinding.btnPush.setVisibility(View.GONE)
            sdkConfigured = false
            releasePlayer()
        })

        viewBinding.subtitlesButton.setOnClickListener {
            val subtitles = ArrayList<String>()
            val subtitlesList = ArrayList<String>()
            for (group in player!!.currentTracks.groups) {
                if (group.type == C.TRACK_TYPE_TEXT) {
                    val groupInfo = group.mediaTrackGroup
                    for (i in 0 until groupInfo.length) {
                        subtitles.add(groupInfo.getFormat(i).language.toString())
                        subtitlesList.add(
                            "${subtitlesList.size + 1}. " + Locale(groupInfo.getFormat(i).language.toString()).displayLanguage
                                    + " (${groupInfo.getFormat(i).label})"
                        )
                    }
                }
            }
            val tempTracks = subtitlesList.toArray(arrayOfNulls<CharSequence>(subtitlesList.size))
            val context: Context = ContextThemeWrapper(this, R.style.AppTheme2)
            val sDialog = MaterialAlertDialogBuilder(context)
                .setTitle(getString(R.string.select_subtitles))
                .setOnCancelListener { playVideo() }
                .setPositiveButton(getString(R.string.off_subtitles)) { self, _ ->
                    trackSelector!!.setParameters(
                        trackSelector!!.buildUponParameters().setRendererDisabled(
                            C.TRACK_TYPE_VIDEO, true
                        )
                    )
                    self.dismiss()
                }
                .setItems(tempTracks) { _, position ->
                    Snackbar.make(
                        viewBinding.root,
                        subtitlesList[position] + getString(R.string.selected),
                        3000
                    ).show()
                    trackSelector!!.setParameters(
                        trackSelector!!.buildUponParameters()
                            .setRendererDisabled(C.TRACK_TYPE_VIDEO, false)
                            .setPreferredTextLanguage(subtitles[position])
                    )
                }
                .create()
            sDialog.show()
            sDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.WHITE)
            sDialog.window?.setBackgroundDrawable(ColorDrawable(0x99000000.toInt()))
        }

        viewBinding.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                val currentVolumeProgress = i / 10.0
                player?.volume = currentVolumeProgress.toFloat()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })


        //new
        viewBinding.qaButton.setOnClickListener({
            viewBinding.qaButton.setBackgroundResource(R.drawable.rounded_background_white_with_border)
            viewBinding.productionButton.setBackgroundResource(R.drawable.round_white_background)
            viewBinding.connectingToUrlValue.setText(stagingUrl)
        })

        viewBinding.productionButton.setOnClickListener({
            viewBinding.productionButton.setBackgroundResource(R.drawable.rounded_background_white_with_border)
            viewBinding.qaButton.setBackgroundResource(R.drawable.round_white_background)
            viewBinding.connectingToUrlValue.setText(productionUrl)
        })

        viewBinding.customVideoInputText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                url = if (charSequence.length > 0) {
                    viewBinding.videoTypeSpinner.setEnabled(false)
                    charSequence.toString()
                } else {
                    viewBinding.videoTypeSpinner.setEnabled(true)
                    resources.getStringArray(R.array.video_urls)[0]
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })


        viewBinding.customAdInputTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (charSequence.length > 0) {
                    viewBinding.addTypeSpinner.setEnabled(false)
                    adsUrl = charSequence.toString()
                } else {
                    viewBinding.addTypeSpinner.setEnabled(true)
                    adsUrl = resources.getStringArray(R.array.add_types_values)[0]
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })

        initVideoTypeSpinner()
        initAddTypesSpinner()
        initCustomDataPointSpinner()
    }

    private fun playAd() {
        Toast.makeText(this, getString(R.string.not_currently_supported), Toast.LENGTH_LONG)
            .show()
    }

    private fun configureSDKAndPlayer() {
        initializePlayer()
        setupDataZoom(player!!)
    }

    private fun configureSDKAndPlayerWithoutExoInstance() {
        setupDataZoomWithoutExoInstance()
    }

    private fun playVideo() {
        setPlayerVideoItem()
        player!!.playWhenReady = playWhenReady
        player!!.prepare()
    }

    private fun setupDataZoom(mPlayer: ExoPlayer) {
        val configId: String = viewBinding.txtConfiguration.getText().toString()
        val configUrl: String = viewBinding.connectingToUrlValue.getText().toString()

        eventCollectorOne = ExoPlayerCollector().create(mPlayer, this@PlayerActivity, "2.18.2")
        eventCollectorOne!!.setConfig(DatazoomConfig(configId, configUrl))
        eventCollectorOne!!.connect(object : DZBeaconConnector.ConnectionListener {
            override fun onSuccess(dzEventCollector: DZEventCollector) {
                sdkConfigured = true
                eventCollectorOne!!.startRecordingEvents(true)
                viewBinding.btnPush.setVisibility(View.VISIBLE)
                setupCustomMetadata()
                setupCustomEvent(dzEventCollector)
                viewBinding.btnPush.setOnClickListener({ v: View? ->
                    try {
                        val event1 = Event(
                            "btnPush", JSONArray(
                                "[{\"customPlay\": \"true\"}]  "
                            )
                        )
                        dzEventCollector.addCustomEvent(event1)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                })
                viewBinding.btnPushCustomEvent.setOnClickListener { v ->
                    val event1 = Event("buttonClick", JSONArray())
                    dzEventCollector.addCustomEvent(event1)
                }
                viewBinding.pbBuffering.setVisibility(View.GONE)
                eventCollectorOne!!.setPlayer(mPlayer)
            }

            override fun onError(t: Throwable) {
                viewBinding.pbBuffering.setVisibility(View.GONE)
                viewBinding.btnSubmit.setVisibility(View.VISIBLE)
                viewBinding.btnSubmit.setEnabled(true)
                showAlert(
                    "Error", ("Error while creating ExoPlayerConnector," +
                            " error:" + t.message)
                )
            }
        })
    }

    private lateinit var mdzEventCollector: DZEventCollector
    private fun setupDataZoomWithoutExoInstance() {
        val configId: String = viewBinding.txtConfiguration.getText().toString()
        val configUrl: String = viewBinding.connectingToUrlValue.getText().toString()
        eventCollectorTwo = ExoPlayerCollector().create(this@PlayerActivity)
        eventCollectorTwo!!.setConfig(DatazoomConfig(configId, configUrl))
        eventCollectorTwo!!.connect(object : DZBeaconConnector.ConnectionListener {
            override fun onSuccess(dzEventCollector: DZEventCollector) {
                mdzEventCollector = dzEventCollector
                sdkConfigured = true
                viewBinding.btnPush.setVisibility(View.VISIBLE)
                setupCustomMetadata()
                setupCustomEventWithoutExoInstance(dzEventCollector)
                eventCollectorTwo!!.startRecordingEvents(true)
                viewBinding.btnPush.setOnClickListener({ v: View? ->
                    try {
                        val event1 = Event(
                            "btnPush", JSONArray(
                                "[{\"customPlay\": \"true\"}]  "
                            )
                        )
                        dzEventCollector.addCustomEvent(event1)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                })
                viewBinding.btnSendCustomEvent.setOnClickListener {
                    try {
                        val event1 = Event(
                            "btnPush", JSONArray(
                                "[{\"customPlay_Without_Player_Instance\": \"true\"}]  "
                            )
                        )
                        dzEventCollector.addCustomEvent(event1)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                viewBinding.pbBuffering.setVisibility(View.GONE)
            }

            override fun onError(t: Throwable) {
                viewBinding.pbBuffering.setVisibility(View.GONE)
                viewBinding.btnSubmit.setVisibility(View.VISIBLE)
                viewBinding.btnSubmit.setEnabled(true)
                showAlert(
                    "Error", ("Error while creating ExoPlayerConnector," +
                            " error:" + t.message)
                )
            }
        })
    }

    private fun setupCustomMetadata() {
        try {
            DZEventCollector.setCustomMetadata(
                JSONArray(
                    "["
                            + "{\"custom_player_name\": \"Android Exo Media Player\"},"
                            + "{\"custom_domain\": \"demo.datazoom.io\"}"
                            + "]"
                )
            )
        } catch (e: JSONException) {
            Log.e(TAG, "Error setting custom metadata", e)
        }
    }

    private fun setupCustomEvent(dzEventCollector: DZEventCollector) {
        val event = Event("Datazoom_EXO_SDK_Loaded", JSONArray())
        dzEventCollector.addCustomEvent(event)
        viewBinding.btnPush.setOnClickListener { v ->
            val event1 = Event("buttonClick", JSONArray())
            dzEventCollector.addCustomEvent(event1)
        }

        viewBinding.btnPushCustomEvent.setOnClickListener { v ->
            val event1 = Event("buttonClick", JSONArray())
            dzEventCollector.addCustomEvent(event1)
        }
    }

    private fun setupCustomEventWithoutExoInstance(dzEventCollector: DZEventCollector) {
        val event = Event("Datazoom_EXO_SDK_Loaded_Without_Exo_Instance", JSONArray())
        dzEventCollector.addCustomEvent(event)
        viewBinding.btnPush.setOnClickListener { v ->
            val event1 = Event("buttonClick", JSONArray())
            dzEventCollector.addCustomEvent(event1)
        }
    }

    private fun showAlert(s: String, s1: String) {
        val builder: AlertDialog.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert)
        } else {
            builder = AlertDialog.Builder(this)
        }
        builder.setTitle(s)
            .setMessage(s1)
            .setPositiveButton(android.R.string.yes) { dialog, which -> }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()

    }


    private fun initializePlayer() {
        adsLoader = ImaAdsLoader.Builder(this)
            .setAdEventListener {
                eventCollectorOne?.trackIMAAdEvent(it)
            }
            .setAdErrorListener {
                eventCollectorOne?.trackIMAAdErrorEvent(it)
            }
            .build()

        val dataSourceFactory: DataSource.Factory = DefaultDataSource.Factory(this)
        val mediaSourceFactory: MediaSource.Factory
        if (adsUrl.isNullOrEmpty()) {
            mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
        } else {
            mediaSourceFactory = DefaultMediaSourceFactory(dataSourceFactory)
                .setLocalAdInsertionComponents(
                    { unusedAdTagUri: MediaItem.AdsConfiguration? -> adsLoader },
                    viewBinding.videoView
                )
        }
        trackSelector = DefaultTrackSelector(this)
        player = ExoPlayer.Builder(this)
            .setMediaSourceFactory(mediaSourceFactory)
            .setTrackSelector(trackSelector!!)
            .build()

    }

    private fun setPlayerVideoItem() {
        if (player != null) {
            player.also { exoPlayer ->
                viewBinding.videoView.player = exoPlayer
                adsLoader!!.setPlayer(exoPlayer)

                // This is for player list
                var listOfItems = ArrayList<MediaItem>()
                listOfItems.add(createMediaItem(url!!))
                if (appVideoType == AppVideoType.PLAYLIST) {
                    listOfItems.add(createMediaItem(getString(R.string.video_for_playlist)))
                }
                exoPlayer!!.addMediaItems(listOfItems)

                // This is for single item
                // exoPlayer!!.setMediaItem(createMediaItem(url!!))
            }.also { player = it }
        }
    }

    private fun pausePlayer() {
        if (player != null) {
            player!!.playWhenReady = false
            player!!.playbackState
        }
    }

    private fun startPlayer() {
        if (player != null) {
            player!!.playWhenReady = true
            player!!.playbackState
        }
    }

    override fun onPause() {
        super.onPause()
        pausePlayer()
    }

    override fun onStop() {
        super.onStop()
        eventCollectorOne?.onStop()
        eventCollectorTwo?.onStop()
    }

    override fun onResume() {
        super.onResume()
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        startPlayer()
    }

    private fun releasePlayer() {
        player?.run {
            stop()
            currentWindow = currentWindow
            playWhenReady = playWhenReady
            release()
        }
        player = null
    }

    private fun initVideoTypeSpinner() {
        // Default value
        url = resources.getStringArray(R.array.video_urls)[0]
        checkCustomVideoUrl()
        appVideoType = AppVideoType.MP4
        setPlayerVideoItem()
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.video_types)
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        viewBinding.videoTypeSpinner.setAdapter(adapter)
        viewBinding.videoTypeSpinner.setSelection(0, false)
        viewBinding.videoTypeSpinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                url = resources.getStringArray(R.array.video_urls)[position]
                checkCustomVideoUrl()
                if (sdkConfigured) {
                    viewBinding.customVideoInputText.clearFocus()
                }

                when (position) {
                    0 -> {
                        appVideoType = AppVideoType.MP4
                        setPlayerVideoItem()
                    }
                    1 -> {
                        appVideoType = AppVideoType.PLAYLIST
                        setPlayerVideoItem()
                    }
                    2 -> {
                        appVideoType = AppVideoType.HLS
                        setPlayerVideoItem()
                    }
                    3 -> {
                        appVideoType = AppVideoType.ERROR
                        setPlayerVideoItem()
                    }
                    4 -> {
                        appVideoType = AppVideoType.LIVE
                        setPlayerVideoItem()
                    }
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        })
    }

    private fun initCustomDataPointSpinner() {
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.custom_datapoint_array)
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        viewBinding.customDataPointSpinner.setAdapter(adapter)
        viewBinding.customDataPointSpinner.setSelection(0, false)
        viewBinding.customDataPointSpinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                handleCustomDataPointAction(position)
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        })
    }


    private fun createMediaItem(videoUrl: String): MediaItem {
        lateinit var mediaMetadata: com.google.android.exoplayer2.MediaMetadata

        when (appVideoType) {
            AppVideoType.MP4 -> {
                mediaMetadata = com.google.android.exoplayer2.MediaMetadata.Builder()
                    .setTitle(getString(R.string.big_buck_bunny))
                    .setSubtitle(getString(R.string.subtitle) + getString(R.string.big_buck_bunny))
                    .build()
            }
            AppVideoType.PLAYLIST -> {
                mediaMetadata = com.google.android.exoplayer2.MediaMetadata.Builder()
                    .build()
            }
            AppVideoType.HLS -> {
                mediaMetadata = com.google.android.exoplayer2.MediaMetadata.Builder()
                    .build()

            }
            AppVideoType.ERROR -> {
                mediaMetadata = com.google.android.exoplayer2.MediaMetadata.Builder()
                    .build()
            }
            AppVideoType.LIVE -> {
                mediaMetadata = com.google.android.exoplayer2.MediaMetadata.Builder()
                    .setTitle(getString(R.string.live_video_title))
                    .setSubtitle(getString(R.string.subtitle) + getString(R.string.live_video_title))
                    .build()
            }
        }

        val assetSrtUri = Uri.parse(("file:///android_asset/subtitle.srt"))
        val subtitleEn = MediaItem.SubtitleConfiguration.Builder(assetSrtUri)
            .setMimeType(MimeTypes.APPLICATION_SUBRIP)
            .setLanguage("en")
            .setSelectionFlags(C.SELECTION_FLAG_DEFAULT)
            .build()

        val subtitleIta = MediaItem.SubtitleConfiguration.Builder(assetSrtUri)
            .setMimeType(MimeTypes.APPLICATION_SUBRIP)
            .setLanguage("it")
            .setSelectionFlags(C.SELECTION_FLAG_DEFAULT)
            .build()

        var subtitleList = ArrayList<MediaItem.SubtitleConfiguration>()
        subtitleList.add(subtitleEn)
        subtitleList.add(subtitleIta)


        val mediaItem: MediaItem
        if (adsUrl.isNullOrEmpty()) {
            mediaItem = MediaItem.Builder()
                .setUri(videoUrl)
                .setSubtitleConfigurations(subtitleList)
                .setMediaMetadata(mediaMetadata)
                .build()
        } else {
            mediaItem = MediaItem.Builder()
                .setUri(videoUrl)
                .setSubtitleConfigurations(subtitleList)
                .setMediaMetadata(mediaMetadata)
                .setAdTagUri(adsUrl)
                .build()
        }

        return mediaItem
    }

    private fun checkCustomVideoUrl() {
        if (!viewBinding.customVideoInputText.getText().toString().isEmpty()) {
            url = viewBinding.customVideoInputText.getText().toString()
        }
    }

    private fun checkCustomAdUrl() {
        if (!viewBinding.customAdInputTxt.getText().toString().isEmpty()) {
            adsUrl = viewBinding.customAdInputTxt.getText().toString()
        } else {
            viewBinding.customAdInputTxt.clearFocus()
        }
    }

    private fun initAddTypesSpinner() {
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.add_types_titles)
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        viewBinding.addTypeSpinner.setAdapter(adapter)
        // Default value
        adsUrl = resources.getStringArray(R.array.add_types_values)[0]
        checkCustomAdUrl()
        viewBinding.addTypeSpinner.setSelection(0, false)
        viewBinding.addTypeSpinner.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                adsUrl = resources.getStringArray(R.array.add_types_values)[position]
                checkCustomAdUrl()
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {}
        })
    }

    private fun handleCustomDataPointAction(action: Int) {
        when (action) {
            CMConstants.ADD_APP_SESSION_METADATA -> {
                CMDialogHandler.addAppSessionMetadataDialog(this)
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.ADD_PLAYER_SESSION_METADATA -> {
                CMDialogHandler.addPlayerSessionMetadataDialog(this)
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.READ_APP_SESSION_METADATA -> {
                Toast.makeText(
                    this,
                    DZEventCollector.getAppSessionCustomMetadata().toString(),
                    Toast.LENGTH_LONG
                ).show()
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.READ_PLAYER_SESSION_METADATA -> {
                Toast.makeText(
                    this,
                    DZEventCollector.getPlayerSessionCustomMetadata().toString(),
                    Toast.LENGTH_LONG
                ).show()
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.DELETE_APP_SESSION_METADATA -> {
                DZEventCollector.clearAppSessionCustomMetadata()
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.DELETE_PLAYER_SESSION_METADATA -> {
                DZEventCollector.clearPlayerSessionCustomMetadata()
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.UPDATE_APP_SESSION_METADATA -> {
                CMDialogHandler.updateAppSessionMetadataDialog(this)
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.UPDATE_PLAYER_SESSION_METADATA -> {
                CMDialogHandler.updatePlayerSessionMetadataDialog(this)
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.ADD_CUSTOM_METADATA_FOR_EXPLICINT_EVENT -> {
                CMDialogHandler.addMetadataForExplicitEventDialog(this)
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
            CMConstants.DELETE_CUSTOM_METADATA_FOR_EXPLICINT_EVENT -> {
                DZEventCollector.clearMetadataForAppropriateEvent()
                viewBinding.customDataPointSpinner.setSelection(0, false)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        eventCollectorOne?.clearPlayerData()
    }
}