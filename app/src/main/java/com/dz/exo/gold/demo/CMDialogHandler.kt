package com.dz.exo.gold.demo

import android.app.Activity
import android.app.AlertDialog
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.dz.collector.android.collector.DZEventCollector
import com.dz.collector.android.eventtypes.EventType
import java.util.*

class CMDialogHandler {
    companion object {

        private var addAppSessionMetadata: AlertDialog? = null

        fun addAppSessionMetadataDialog(activity: Activity) {
            if (addAppSessionMetadata != null) {
                addAppSessionMetadata!!.dismiss()
            }
            val dialogBuilder =
                AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.custom_metadata_dialog, null)
            val key_1 = dialogView.findViewById<EditText>(R.id.key_1)
            val value_1 = dialogView.findViewById<EditText>(R.id.value_1)
            val addMetadataButton = dialogView.findViewById<Button>(R.id.addMetadataButton)
            addMetadataButton.setOnClickListener {
                if (!key_1.text.toString().isEmpty() && !value_1.text.toString().isEmpty()) {
                    val appSessionMapTest = HashMap<String, Any>()
                    appSessionMapTest[key_1.text.toString()] = value_1.text.toString()
                    DZEventCollector.setAppSessionCustomMetadata(appSessionMapTest)
                    addAppSessionMetadata!!.dismiss()
                    addAppSessionMetadata = null
                } else {
                    Toast.makeText(
                        activity,
                        activity.getString(R.string.please_insert_key_value_pair),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            val cancel = dialogView.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener {
                addAppSessionMetadata!!.dismiss()
                addAppSessionMetadata = null
            }
            dialogBuilder.setView(dialogView)
            addAppSessionMetadata = dialogBuilder.create()
            addAppSessionMetadata!!.setCancelable(false)
            addAppSessionMetadata!!.show()
        }


        private var addPlayerSessionMetadata: AlertDialog? = null

        fun addPlayerSessionMetadataDialog(activity: Activity) {
            if (addPlayerSessionMetadata != null) {
                addPlayerSessionMetadata!!.dismiss()
            }
            val dialogBuilder =
                AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.custom_metadata_dialog, null)
            val key_1 = dialogView.findViewById<EditText>(R.id.key_1)
            val value_1 = dialogView.findViewById<EditText>(R.id.value_1)
            val addMetadataButton = dialogView.findViewById<Button>(R.id.addMetadataButton)
            addMetadataButton.setOnClickListener {
                if (!key_1.text.toString().isEmpty() && !value_1.text.toString().isEmpty()) {
                    val appPlayerMapTest = HashMap<String, Any>()
                    appPlayerMapTest[key_1.text.toString()] = value_1.text.toString().isEmpty()
                    DZEventCollector.setPlayerSessionCustomMetadata(appPlayerMapTest)
                    addPlayerSessionMetadata!!.dismiss()
                    addPlayerSessionMetadata = null
                } else {
                    Toast.makeText(
                        activity,
                        activity.getString(R.string.please_insert_key_value_pair),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            val cancel = dialogView.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener {
                addPlayerSessionMetadata!!.dismiss()
                addPlayerSessionMetadata = null
            }
            dialogBuilder.setView(dialogView)
            addPlayerSessionMetadata = dialogBuilder.create()
            addPlayerSessionMetadata!!.setCancelable(false)
            addPlayerSessionMetadata!!.show()
        }

        private var updateAppSessionMetadata: AlertDialog? = null
        fun updateAppSessionMetadataDialog(activity: Activity) {
            if (updateAppSessionMetadata != null) {
                updateAppSessionMetadata!!.dismiss()
            }
            val dialogBuilder =
                AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.custom_metadata_dialog, null)
            val key_1 = dialogView.findViewById<EditText>(R.id.key_1)
            val value_1 = dialogView.findViewById<EditText>(R.id.value_1)
            val addMetadataButton = dialogView.findViewById<Button>(R.id.addMetadataButton)
            addMetadataButton.setOnClickListener {
                if (!key_1.text.toString().isEmpty() && !value_1.text.toString().isEmpty()) {
                    DZEventCollector.updateAppSessionCustomMetadata(
                        key_1.text.toString(),
                        value_1.text.toString()
                    )
                    updateAppSessionMetadata!!.dismiss()
                    updateAppSessionMetadata = null
                } else {
                    Toast.makeText(
                        activity,
                        activity.getString(R.string.please_insert_key_value_pair),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            val cancel = dialogView.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener {
                updateAppSessionMetadata!!.dismiss()
                updateAppSessionMetadata = null
            }
            dialogBuilder.setView(dialogView)
            updateAppSessionMetadata = dialogBuilder.create()
            updateAppSessionMetadata!!.setCancelable(false)
            updateAppSessionMetadata!!.show()
        }

        private var updatePalyerSessionMetadata: AlertDialog? = null
        fun updatePlayerSessionMetadataDialog(activity: Activity) {
            if (updatePalyerSessionMetadata != null) {
                updatePalyerSessionMetadata!!.dismiss()
            }
            val dialogBuilder =
                AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.custom_metadata_dialog, null)
            val key_1 = dialogView.findViewById<EditText>(R.id.key_1)
            val value_1 = dialogView.findViewById<EditText>(R.id.value_1)
            val addMetadataButton = dialogView.findViewById<Button>(R.id.addMetadataButton)
            addMetadataButton.setOnClickListener {
                if (!key_1.text.toString().isEmpty() && !value_1.text.toString().isEmpty()) {
                    DZEventCollector.updatePlayerSessionCustomMetadata(
                        key_1.text.toString(),
                        value_1.text.toString()
                    )
                    updatePalyerSessionMetadata!!.dismiss()
                    updatePalyerSessionMetadata = null
                } else {
                    Toast.makeText(
                        activity,
                        activity.getString(R.string.please_insert_key_value_pair),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            val cancel = dialogView.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener {
                updatePalyerSessionMetadata!!.dismiss()
                updatePalyerSessionMetadata = null
            }
            dialogBuilder.setView(dialogView)
            updatePalyerSessionMetadata = dialogBuilder.create()
            updatePalyerSessionMetadata!!.setCancelable(false)
            updatePalyerSessionMetadata!!.show()
        }

        private var metadataForExplicitEventDialog: AlertDialog? = null
        fun addMetadataForExplicitEventDialog(activity: Activity) {
            if (metadataForExplicitEventDialog != null) {
                metadataForExplicitEventDialog!!.dismiss()
            }
            val dialogBuilder =
                AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            val inflater = activity.layoutInflater
            val dialogView: View = inflater.inflate(R.layout.custom_metadata_dialog, null)
            val key_1 = dialogView.findViewById<EditText>(R.id.key_1)
            val value_1 = dialogView.findViewById<EditText>(R.id.value_1)
            val addMetadataButton = dialogView.findViewById<Button>(R.id.addMetadataButton)
            addMetadataButton.setOnClickListener {
                if (!key_1.text.toString().isEmpty() && !value_1.text.toString().isEmpty()) {
                    val testPauseMap = HashMap<String, Any>()
                    testPauseMap[key_1.text.toString()] = value_1.text.toString()
                    DZEventCollector.addMetadataForAppropriateEvent(
                        EventType.PAUSE,
                        testPauseMap
                    )
                    metadataForExplicitEventDialog!!.dismiss()
                    metadataForExplicitEventDialog = null
                } else {
                    Toast.makeText(
                        activity,
                        activity.getString(R.string.please_insert_key_value_pair),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            val cancel = dialogView.findViewById<Button>(R.id.cancel)
            cancel.setOnClickListener {
                metadataForExplicitEventDialog!!.dismiss()
                metadataForExplicitEventDialog = null
            }
            dialogBuilder.setView(dialogView)
            metadataForExplicitEventDialog = dialogBuilder.create()
            metadataForExplicitEventDialog!!.setCancelable(false)
            metadataForExplicitEventDialog!!.show()
        }

    }
}