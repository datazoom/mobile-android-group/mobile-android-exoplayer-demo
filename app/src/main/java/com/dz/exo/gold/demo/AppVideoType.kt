package com.dz.exo.gold.demo

class AppVideoType {
    companion object {
        val MP4 = "MP4"
        val PLAYLIST = "PLAYLIST"
        val HLS = "HLS"
        val ERROR = "ERROR"
        val LIVE = "LIVE"
    }
}